
var request = new XMLHttpRequest();
request.open('GET', 'data/books.json', false);
request.send(null);
var data = JSON.parse(request.responseText);
console.log(data);

var books = data.books;


var list = document.createElement('ol');
for (var i=0; i < books.length; i++) {
	console.log(books[i].title);
	var item = document.createElement('li');
	item.innerHTML = books[i].title;
	list.appendChild(item);
}
document.body.appendChild(list);

var list1 = document.createElement('ul');
for (var i=0; i < books.length; i++) {
	console.log(books[i].title + ', ' + books[i].year);
	var item1 = document.createElement('li');
	item1.innerHTML = books[i].title + ', ' + books[i].year;
	list1.appendChild(item1);
} 
document.body.appendChild(list1);

var table = document.createElement('table');
table.setAttribute('id','thetable');
for (var i=0; i < books.length; i++) {
	var tr = document.createElement('tr');
	tr.setAttribute('id','tr' + i);
		var td1 = document.createElement('td');
		var td2 = document.createElement('td');
		var td3 = document.createElement('td');
		var td4 = document.createElement('td');
		td1.appendChild(document.createTextNode(books[i].title));
		td2.appendChild(document.createTextNode(books[i].year));
		td3.appendChild(document.createTextNode(books[i].authors));
		td4.appendChild(document.createTextNode(books[i].isbn));
		tr.appendChild(td1);
		tr.appendChild(td2);
		tr.appendChild(td3);
		tr.appendChild(td4);
	table.appendChild(tr);
} 
	var header = table.createTHead();
	var row = header.insertRow(0);
	var cellISBN = row.insertCell(0);
	var cellAuthors = row.insertCell(0);
	var cellYear = row.insertCell(0);
	var cellTitle = row.insertCell(0);
	cellTitle.innerHTML = "<b>Title</b>";
	cellYear.innerHTML = "<b>Year</b>";
	cellAuthors.innerHTML = "<b>Authors</b>";
	cellISBN.innerHTML = "<b>ISBN</b>";
document.body.appendChild(table);

function addRowHandlers() {
    var table = document.getElementById('thetable');
    var rows = table.getElementsByTagName('tr');
    for (i = 1; i < rows.length; i++) {
        var currentRow = table.rows[i];
        var createClickHandler = 
            function(row) {
                return function() {
                	div = document.getElementById('divH1');
					var title = document.getElementById('title');
					if (!title) {
						title = document.createElement('h1');
						title.setAttribute('id','title');
						div.appendChild(title);
					}
					var cell = row.getElementsByTagName('td')[0];
					title.innerHTML = cell.innerHTML;
					};
            };
        currentRow.onclick = createClickHandler(currentRow);
    }
}

window.onload = addRowHandlers();
